LV2 ports by Jeremy Salwen 2010-2020

This is a package of three LV2 plugin synthesizers.  To use them, you need an
LV2 host which works with MIDI synthesizers.  The different parameters (listed
below for each synthesizer) can be controlled either from MIDI CC events through
the "midi in" port, or they can be controlled using the LV2 control ports.  
These two options are exclusive, and can be switched between by the 
"Control Mode" option.

For installation instructions, see the INSTALL file.

This software is licensed under the GNU GPL V3 see LICENSE for details.
Bug reports, questions and death threats to someone@d00m.org and/or
jeremysalwen@gmail.com.

The following are relevant excerpts from the original synthesizers released by
50m30n3.


*******SO-666 v.1.0 by 50m30n3 2009*******
http://d00m.org/~someone/so666/

SO-666 is a feedback based drone synthesizer building upon the SO-KL5 synth.
It creates haunting cacophonic howls and drones.
It's a bit hard to play but making good use of the modwheel will help keep the
sound in control.

***How to play***

Since it can be complicated to get any sound from this thing whatsoever you can try this:

    Turn your modwheel all the way down
    Press any key, keep it pressed
    Slowly turn up your modwheel to increase feedback
    About 2/3 up you should start to hear something
    Put the modwheel in center position for maximum playability
    Play some different notes or chords (dissonant intervals work great since they create a lot of low beat frequencies)
    Twiddle the modwheel, cutoff and resonance for different effects
    ...
    Profit!


Parameter list:
CC#1	-	Feedback (mod wheel)
CC#7	-	Volume
CC#71	-	Filter Resonance
CC#74	-	Filter Cutoff



*******SO-KL5 v.1.1 by 50m30n3 2010*******
http://d00m.org/~someone/sokl5/

SO-KL5 is a string based "piano" synthezizer. The strings are
modeled using the Karplus-Strong String simulation method.

Parameter list:
CC#1	-	Sustain
CC#7	-	Volume
CC#64	-	Sustain
CC#71	-	Filter Resonance
CC#74	-	Filter Cutoff

Sustain can be controlled by CC#1 (Mod Wheel) and CC#64 (Sustain) to make
control easier for people without a sustain pedal.

*******SO-404 v.1.2 by 50m30n3 2009-2011*******
http://d00m.org/~someone/so404/

SO-404 is a simple bass synthesizer using 1 oscillator and 1 filter.
The oscillator is a simple saw wave oscillator and the filter is a simple
resonant lowpass filter. You know, like that other very good and famous synth
with a similar name. Just not as good and famous.

Parameter list:
CC#7	-	Volume
CC#65	-	Portamento time
CC#71	-	Filter Resonance
CC#72	-	Release time
CC#74	-	Filter Cutoff
CC#79	-	Filter Envelope

If two notes are played together the pitch will slide according to the
portamento time. Filter cutoff is influenced by the MIDI note velocity.
A note velocity above 100 generates an accented note where the amp EG is
replaced by the filter EG.
